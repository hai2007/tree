# tree
树图组件，包括矩形、圆形等常见的树图。

<p align='center'><img src='./view-LR.png'></p>
<p align='center'><img src='./view-circle.png'></p>

<p>
  <a href="https://hai2007.gitee.io/npm-downloads?interval=7&packages=@clunch/tree"><img src="https://img.shields.io/npm/dm/@clunch/tree.svg" alt="downloads"></a>
  <a href="https://www.npmjs.com/package/@clunch/tree"><img src="https://img.shields.io/npm/v/@clunch/tree.svg" alt="Version"></a>
  <a href="https://github.com/clunch-contrib/tree/graphs/commit-activity" target='_blank'><img alt="GitHub repo commit" src="https://img.shields.io/github/last-commit/clunch-contrib/tree"></a>
  <a href="https://github.com/clunch-contrib/tree/blob/master/LICENSE"><img src="https://img.shields.io/npm/l/@clunch/tree.svg" alt="License"></a>
  <a href="https://github.com/clunch-contrib/tree" target='_blank'><img alt="GitHub repo stars" src="https://img.shields.io/github/stars/clunch-contrib/tree?style=social"></a>
</p>

## 如何使用？

首先，需要安装npm包（目前我们仅支持npm方式管理）：

```
npm install --save clunch @clunch/tree
```

然后注册组件：

```js
import Clunch from 'clunch';
import tree from '@clunch/tree';

Clunch.series('ui-tree',tree);
```

然后，你就可以使用这个组件了：

```html
<ui-tree data='json' />
```

- data:数据，可以是任意格式（比如：``` [{name:"",children:[...]},...] ``` 默认支持，别的格式需要额外配置解析方法）

除了上面的必输项外，还有下列可选项：

|属性|类型|描述|默认值|可选值|
|----|----|----|----|----|
|format|json|配置数据格式（详情见下面）|（见下面）||
|type|string|树图类型| TB |LR \| RL \| TB \| BT \| circle|
|x|number|图形左上角的横坐标（type不为circle时有效）|10||
|y|number|图形左上角的纵坐标（type不为circle时有效）|10||
|width|number|图形宽（type不为circle时有效）|画布宽减20||
|height|number|图形高（type不为circle时有效）|画布高减20||
|cx|number|图形中心的横坐标（type为circle时有效）|画布中心横坐标||
|cy|number|图形中心的纵坐标（type为circle时有效）|画布中心纵坐标||
|radius|number|图形半径（type为circle时有效）|50%的画布宽(高)减100||
|begin-deg|number|图形开始弧度（type为circle时有效）|0||
|deg|number|图形跨越的弧度（type为circle时有效）|2PI||

### 【数据格式format】

因为用户提供的原始数据可能不是按照我们设计的格式提供，为了兼容所有的数据格式，我们提供了```format配置项```用以告诉组件你的数据应该如何解析：

```js
format:{
    "root":function(initTree){ /*返回根结点*/ },
    "id":function(treedata){ /*返回id*/ },
    "child":function(parentTree, initTree){ /*返回孩子结点*/ }
}
```

你一共可以配置三项，分别用以告诉我们哪个是根节点、一个结点的唯一标识如何生成和如何获取孩子结点。

比如我们内置数据格式的话，可以这样配置：

```js
// 默认配置
format:{
    "root":function(initTree){  return initTree; },
    "id":function(treedata){ return treedata.name; },
    "child":function(parentTree, initTree){ return parentTree.children; }
}
```

上面三项均可选，你只需要配置和默认数据格式解析不一样的项即可，无需全部配置。

由于此组件是基于[Clunch](https://github.com/hai2007/clunch)开发的，我们默认你已经掌握了相关知识。

[<< 你可以点击此处学习Clunch.js如何使用](https://hai2007.gitee.io/clunch/#/course/introduce?fixed=top)

开源协议
---------------------------------------
[MIT](https://github.com/clunch-contrib/tree/blob/master/LICENSE)

Copyright (c) 2021 [hai2007](https://hai2007.gitee.io/sweethome/) 走一步，再走一步。
