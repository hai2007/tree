import Clunch from 'clunch';
import tree from '../index';
import image from './test.clunch';
import demoData from './data.json';

window.clunch = new (Clunch.series('ui-tree', tree))({
    el: document.getElementById('root'),
    data: function () {
        return {
            data: demoData
        };
    },
    render: image
});
